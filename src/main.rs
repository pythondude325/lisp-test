macro_rules! lisp {
    // Keywords

    // the def keyword indicates an item (def mod, def fn)
    ( ($(#[$meta_item:meta])* def $visibility:vis mod $name:ident $($item:tt)* ) ) => {
        $(#[$meta_item])*
        $visibility mod $name {
            $(lisp!{$item})*
        }
    };

    // Function definition
    ( ($(#[$meta_item:meta])* def $visibility:vis fn $name:ident ( $($arg:ident $(<$type:ty>)?)* ) <$return_type:ty> $s:tt ) ) => {
	    $(#[$meta_item])*
        $visibility fn $name ( $($arg $(: $type)? ),* ) -> $return_type {
            lisp!{$s}
        }
    };
    
    // Struct definition
    ( ($(#[$meta_item:meta])* def $visibility:vis struct $name:ident ( $($field_name:ident <$field_type:ty>)* ) ) ) => (
        $(#[$meta_item])*
        $visibility struct $name {
            $( $field_name: $field_type ),*
        }
    );
    
    // Impl block
    ( ($(#[$meta_item:meta])* def impl $name:ident $($item:tt)*) ) => (
        $(#[$meta_item])*
        impl $name {
            $(lisp!{$item})*
        }
    );

    // Let block (used to assign variables)
    ( (let ( $( $name:ident = $val:tt )* ) $( $s:tt )+) ) => {
        {
            $( let $name = lisp!($val); )*
            $( lisp!{$s} );*
        }
    };

    // Essentially the block expression from rust
    ( (do $( $s:tt )+) ) => (
        { $( lisp!($s) );* }
    );

    // IF $cond THEN $then_branch ELSE $else_branch
    // Use a do expression if you want more than one expression per branch
    ( (cond $cond:tt $then_branch:tt $else_branch:tt) ) => (
        if lisp!($cond) { lisp!($then_branch) } else { lisp!($else_branch) }
    );
    
    // Closure:
    // For closures type annotations are optional and marked with angle brackets
    // But the return type annotation doesn't work yet.
    /*( (fn ( $( $arg:ident $(<$type:ty>)? )* ) $(<$ret_type:ty>)? $e:tt ) ) => (
        | $($arg $(: $type)? ),* | $(-> $ret_type)? { lisp!($e) }
    );*/
    ( (fn | $( $arg:tt )* | $(<$ret_type:ty>)? $e:tt ) ) => (
        | $($arg),* | $(-> $ret_type)? { lisp!($e) }
    );

    // Binary Operators
    ( (== $a:tt $b:tt) ) => ( (lisp!($a)) == (lisp!($b)) );
    ( (!= $a:tt $b:tt) ) => ( (lisp!($a)) != (lisp!($b)) );

    ( (>  $a:tt $b:tt) ) => ( (lisp!($a)) >  (lisp!($b)) );
    ( (>= $a:tt $b:tt) ) => ( (lisp!($a)) >= (lisp!($b)) );
    ( (<  $a:tt $b:tt) ) => ( (lisp!($a)) <  (lisp!($b)) );
    ( (<= $a:tt $b:tt) ) => ( (lisp!($a)) <= (lisp!($b)) );

    ( (+  $a:tt $b:tt) ) => ( (lisp!($a)) +  (lisp!($b)) );
    ( (-  $a:tt $b:tt) ) => ( (lisp!($a)) -  (lisp!($b)) );
    ( (*  $a:tt $b:tt) ) => ( (lisp!($a)) *  (lisp!($b)) );
    ( (/  $a:tt $b:tt) ) => ( (lisp!($a)) /  (lisp!($b)) );
    ( (%  $a:tt $b:tt) ) => ( (lisp!($a)) %  (lisp!($b)) );
    
    ( (<< $a:tt $b:tt) ) => ( (lisp!($a)) << (lisp!($b)) );
    ( (>> $a:tt $b:tt) ) => ( (lisp!($a)) >> (lisp!($b)) );
    ( (&  $a:tt $b:tt) ) => ( (lisp!($a)) &  (lisp!($b)) );
    ( (|  $a:tt $b:tt) ) => ( (lisp!($a)) |  (lisp!($b)) );
    ( (^  $a:tt $b:tt) ) => ( (lisp!($a)) ^  (lisp!($b)) );
    
    ( (.. $a:tt $b:tt) ) => ( (lisp!($a)) .. (lisp!($b)) );
    ( (..=$a:tt $b:tt) ) => ( (lisp!($a)) ..=(lisp!($b)) );

    // Unary operator()
    ( (- $a:tt) ) => ( -lisp!($a) );
    ( (* $a:tt) ) => ( *lisp!($e) );
    ( (& $e:tt) ) => ( &lisp!($e) );
    ( (? $e:tt) ) => ( lisp!($e)? );
    ( (not $e:tt) ) => ( !lisp!($e) );

    // Variadic operators
    ( (and $a:tt $($b:tt)+) ) => ( lisp!{$a} $(&& lisp!($b))+ );
    ( (or $a:tt $($b:tt)+) ) => ( lisp!{$a} $(|| lisp!($b))+ );

    // Tuple constructor
    ( (tuple $($a:tt)*) ) => ( ( $(lisp!{$a}),* ) );
    

    // Macro call
	( ($name:ident! $($arg:tt)*) ) => ( $name!($( lisp!($arg) ),*) );
    
    // Field access expression
    ( (.$name:ident $e:tt) ) => (
        lisp!($e).$name
    );
    
    // Member call expression
    ( (@$name:ident $e:tt $($arg:tt)* ) ) => (
        lisp!($e).$name($(lisp!($arg)),*)
    );

    // Function call
    ( ($f:tt $( $e:tt )*) ) => (
        (lisp!{$f})( $( lisp!{$e} ),* )
    );

    // This is syntax sugar to enable the use of multipart paths as a literal
    // instead of some BS like (:: (:: path to) function)
    // Use like so: ([path::to::function] 2 3)
    ( [$n:path] ) => ($n);
    
    // Anything between curly brackets is just rust code embeded in my lisp code
    ( {$($cheat:tt)*} ) => {$($cheat)*};
    
    ($e:expr) => ($e); 
}

macro_rules! l {
    ( $($s:tt)+ ) => ( $(lisp!{ $s })* );
}

l!{
    (#[cfg(test)] def mod tests
        (#[test] def fn field_access () <()>
            (assert_eq!
                (let (a = (vec! 1 2 3))
                    (* (@len a) 4))
                12))
        (#[test] def fn vec_constructor () <()> 
            (assert_eq!
                (vec! 1 2 3)
                {vec![1, 2, 3]}))
        (#[test] def fn path_access () <()> (do
            {use std::ops::Mul;}
            (assert_eq!
                ([i32::mul] 2 3)
                6)))
        (#[test] def fn closures () <()> (do
            (assert_eq!
                ((fn |x| (* x 2)) 4)
                8)
            (assert_eq!
                ((fn |x: i32| (* x 3)) 5)
                15)))
        (#[test] def fn logic_operators () <()> (do
            (assert_eq! (and true true) true)
            (assert_eq! (and false true) false)
            (assert_eq! (and true true false) false)
            (assert_eq! (or false false) false)
            (assert_eq! (or false true) true)
            (assert_eq! (or false true false) true)
            (assert_eq! (not true) false)
            (assert_eq! (not false) true)))
        (#[test] def fn conditionals () <()> (do
            (assert_eq! (cond (> 5 2) "a" "b") "a")
            (assert_eq! (cond (< 7 3) "1" "2") "2")))
        (#[test] def fn struct_def () <()> (do
            (def struct Point (a <i32> b <i32>))
            (let (x = {Point{a: 3, b: 5}})
                (assert_eq! (.a x) 3)
                (assert_eq! (.b x) 5))
            (def impl Point
                (def fn dot(self other <Point>) <i32> 
                    (+ (* (.a self) (.a other)) (* (.b self) (.b other)))))
            (let (y = {Point{a: 4, b: 4}})
                (assert_eq! (@dot y {Point{a: 3, b: -3}}) 0))))
    )
    
    (def fn permission_string(mode <u32>) <String> 
        (@collect (@flat_map
            (@rev (.. 0 3))
            (fn |i|
                (@map
                    (@zip (@rev (.. 0 3)) (@chars "rwx"))
                    (fn |(j, c)|
                        (cond (!= (& mode (<< 1 (+ j (* i 3)))) 0)
                            c '-')))))))


}

